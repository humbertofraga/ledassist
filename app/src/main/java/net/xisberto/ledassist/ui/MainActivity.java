package net.xisberto.ledassist.ui;

import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import net.xisberto.ledassist.R;
import net.xisberto.ledassist.control.Settings;


public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        TimePickerDialog.OnTimeSetListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String KEY = "key", TARGET = "target", TAG_TIME_PICKER = "radial_picker";
    private static final String TAG_FEEDBACK = "fragment_feedback";

    private SwitchCompat checkLed;
    private String mPreferencesKey;
    private int mTargetButton;
    private AboutFragment aboutFragment;

    public static final String EXTRA_LED_STATUS = "LED_STATUS";

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Settings.ACTION_LED_ENABLED.equals(intent.getAction())
                    && intent.hasExtra(EXTRA_LED_STATUS))
                updateLayout(intent.getBooleanExtra(EXTRA_LED_STATUS, true));
        }
    };

    private void updateLayout(boolean led_enabled) {
        TextView textStatus = findViewById(R.id.textStatus);
        String str_status = getString(led_enabled ? R.string.enabled : R.string.disabled);
        textStatus.setText(getString(R.string.current_led_status, str_status));
        textStatus.setCompoundDrawablesWithIntrinsicBounds(
                led_enabled ? R.drawable.ic_led_on : R.drawable.ic_led_off,
                0, 0, 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                MainActivity.this.getFragmentManager()
//                        .popBackStack();
//                return true;
//            }
//        });

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(KEY)
                    && savedInstanceState.containsKey(TARGET)) {
                mPreferencesKey = savedInstanceState.getString(KEY);
                mTargetButton = savedInstanceState.getInt(TARGET);
            }
        }

        checkLed = findViewById(R.id.checkLed);
        checkLed.setChecked(Settings.isActive(this));
        checkLed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("MainActivity", String.format("checkLed is %b", checkLed.isChecked()));
                Settings.setActive(MainActivity.this, checkLed.isChecked());
            }
        });
        RadioGroup radioGroup = findViewById(R.id.radioGrp);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioAlways:
                        findViewById(R.id.buttonStart).setEnabled(false);
                        findViewById(R.id.buttonEnd).setEnabled(false);
                        Settings.setAlwaysOff(MainActivity.this, true);
                        break;
                    case R.id.radioTimes:
                    default:
                        findViewById(R.id.buttonStart).setEnabled(true);
                        findViewById(R.id.buttonEnd).setEnabled(true);
                        Settings.setAlwaysOff(MainActivity.this, false);
                        break;
                }
            }
        });
        if (Settings.isAlwaysOff(this)) {
            radioGroup.check(R.id.radioAlways);
        } else {
            radioGroup.check(R.id.radioTimes);
        }

        Button buttonStart = findViewById(R.id.buttonStart);
        buttonStart.setOnClickListener(this);
        buttonStart.setText(Settings.getTimeString(this, Settings.KEY_START));

        Button buttonEnd = findViewById(R.id.buttonEnd);
        buttonEnd.setOnClickListener(this);
        buttonEnd.setText(Settings.getTimeString(this, Settings.KEY_END));

        findViewById(R.id.buttonFeedback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (aboutFragment == null) {
                    aboutFragment = new AboutFragment();
                }
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getFragmentManager().beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .replace(R.id.main_content, aboutFragment, TAG_FEEDBACK)
                        .addToBackStack(null)
                        .commit();
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext())
                .unregisterOnSharedPreferenceChangeListener(this);
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(receiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        TimePickerDialog dialog = (TimePickerDialog) getFragmentManager()
                .findFragmentByTag(TAG_TIME_PICKER);
        if (dialog != null) {
            dialog.setOnTimeSetListener(this);
        }

        aboutFragment = (AboutFragment) getFragmentManager()
                .findFragmentByTag(TAG_FEEDBACK);

        updateLayout(Settings.isLedEnabled(this));

        PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext())
                .registerOnSharedPreferenceChangeListener(this);

        LocalBroadcastManager.getInstance(this)
                .registerReceiver(receiver, new IntentFilter(Settings.ACTION_LED_ENABLED));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(KEY, mPreferencesKey);
        outState.putInt(TARGET, mTargetButton);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            getFragmentManager()
                    .popBackStack();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        mTargetButton = v.getId();
        if (v.getId() == R.id.buttonStart) {
            mPreferencesKey = Settings.KEY_START;
        } else {
            mPreferencesKey = Settings.KEY_END;
        }

        int hour = Settings.getHour(this, mPreferencesKey);
        int minute = Settings.getMinute(this, mPreferencesKey);

        TimePickerDialog dialog = TimePickerDialog.newInstance(MainActivity.this,
                hour, minute, DateFormat.is24HourFormat(this));

        dialog.show(getFragmentManager(), TAG_TIME_PICKER);
    }

    /**
     * Will be called when the {@link TimePickerDialog} selects a time
     */
    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        Button button = findViewById(mTargetButton);
        Settings.setTime(this, mPreferencesKey, hourOfDay, minute);
        button.setText(Settings.getTimeString(this, hourOfDay, minute));
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.startsWith(Settings.KEY_START) || key.startsWith(Settings.KEY_END) || key.equals(Settings.KEY_ALWAYS_OFF)) {
            if (checkLed.isChecked()) {
                Settings.setActive(this, true);
            }
        }
    }
}
